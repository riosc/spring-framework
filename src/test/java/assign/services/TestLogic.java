package assign.services;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class TestLogic {
	
	public Logic logic;
	
	@Before
	public void setUp() {
		ArrayList<String> arr = new ArrayList<String>();
		arr.add("http://eavesdrop.openstack.org/meetings/barbican/2013/");
		logic = new Logic("meetings", "barbican", "2013", "Submit", arr);
	}
	
	@Test
	public void testGetUrl() {
		assertEquals(logic.getUrl(), "http://eavesdrop.openstack.org/");
	}
	
	@Test
	public void testGetProject(){
		assertEquals(logic.getProject(), "barbican");
	}
	
	@Test
	public void testGetYear(){
		assertEquals(logic.getYear(), "2013");
	}
	
	@Test
	public void testGetType(){
		assertEquals(logic.getType(), "meetings");
	}
	
	@Test
	public void testGetHist(){
		assertEquals(logic.getHist().get(0), "http://eavesdrop.openstack.org/meetings/barbican/2013/");
	}
	
	
}