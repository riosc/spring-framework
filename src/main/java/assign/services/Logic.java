package assign.services;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

@Service
public class Logic {
	private String type;
	private String project;
	private String year;
	private String act;
	private ArrayList<String> hist = new ArrayList<String>();
	private String url = "http://eavesdrop.openstack.org/";
	
	public Logic(String type, String project, String year, String act, ArrayList<String> hist){
		this.type = type;
		this.project = project;
		this.year = year;
		this.act = act;
		this.hist = hist;
	}

	public Logic(){};

	public void getParam(HttpServletResponse response, HttpServletRequest request) throws IOException{
		if("irclogs".equals(type))
			url += type + "/%23" + project;
		else if("meetings".equals(type))
			url += type + "/" + project + "/" + year;

		if("Submit".equals(act)){
			try{
				Document doc = Jsoup.connect(url).get();
				Elements questions = doc.select("a[href]");
				for(Element link: questions){
					link.attr("href", url+"/"+link.attr("href"));
				}
				Elements images = doc.select("img[src]");
				for(Element link: images){
					link.attr("src", "http://eavesdrop.openstack.org/"+link.attr("src"));
				}
				create(response, request).append(doc.html());
				// it gets here, when it works
			} catch(IOException e){
				create(response, request).append("Url not Found");
			}
		}
	}

	public PrintWriter create(HttpServletResponse response, HttpServletRequest request) throws IOException{
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");

		PrintWriter writer = response.getWriter();
		writer.append("<!DOCTYPE html>")
		.append("<html>")
		.append("    <body>");

		writer.append("<form method=\"POST\" action=\"/redirect\" >");

		writer.append("Enter the type<br/>");
		writer.append("<input type=\"text\" name=\"type\"/><br/>");
		writer.append("Enter the project<br/>");
		writer.append("<input type=\"text\" name=\"project\"/><br/>");
		writer.append("Enter the year<br/>");
		writer.append("<input type=\"text\" name=\"year\"/><br/>");

		writer.append("<input type=\"submit\" name=\"act\" value=\"Submit\"/>");
		writer.append("  <input type=\"submit\" name=\"act\" value=\"Close\"/> <br/>");
		for(int i=0; i<hist.size(); i++)
			writer.append(hist.get(i) + "<br/>");
		writer.append("</form>");

		writer.append("    </body>").append("</html>");

		return writer;
	}

	public void error(HttpServletResponse response, HttpServletRequest request) throws IOException{
		create(response, request).append("<b>Wrong entry on field box. Please try again.</b><br/><br/>");
	}
	
	public String getUrl(){
		return url;
	}
	
	public String getType(){
		return type;
	}
	
	public String getProject(){
		return project;
	}
	
	public String getYear(){
		return year;
	}
	
	public ArrayList<String> getHist(){
		return hist;
	}
}
