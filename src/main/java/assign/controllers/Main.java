package assign.controllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import assign.services.Logic;

@Controller
public class Main {
	private ArrayList<String> hist = new ArrayList<String>();
	
	@RequestMapping(value = "/redirect", params = {"type", "project", "year", "act"}, method=RequestMethod.POST)
	public void getParam(@RequestParam("type") String type, @RequestParam("project") String project, @RequestParam("year") String year,
			@RequestParam("act") String act, HttpServletResponse response, HttpServletRequest request) throws IOException, ServletException {
		HttpSession session = request.getSession(true);

		Logic l = new Logic();
		if("irclogs".equals(type) || "meetings".equals(type)){
			l = new Logic(type, project, year, act, hist);
			l.getParam(response, request);
			hist.add(l.getUrl());
		} else if("Close".equals(act)){
			hist.clear();
			session.invalidate();
			l.create(response, request);
		} else
			l.error(response, request);
	}
}